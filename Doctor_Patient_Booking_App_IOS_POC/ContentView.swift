//
//  ContentView.swift
//  Doctor_Patient_Booking_App_IOS_POC
//
//  Created by Giri Thangellapally on 17/11/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Welcome to Health Care App")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
