//
//  Doctor_Patient_Booking_App_IOS_POCApp.swift
//  Doctor_Patient_Booking_App_IOS_POC
//
//  Created by Giri Thangellapally on 17/11/22.
//

import SwiftUI

@main
struct Doctor_Patient_Booking_App_IOS_POCApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
